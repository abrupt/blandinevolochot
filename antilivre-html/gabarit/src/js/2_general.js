// Scripts

// When page loaded
window.addEventListener('load', function () {
  document.body.classList.add('loaded');
});

// Menu
const menu = document.querySelector('.menu');
const titre = document.querySelector('.pagetitre');

menu.addEventListener('click', (e) => {
  e.preventDefault();
  titre.classList.toggle('show');
  document.querySelector('.icone--info').classList.toggle('rotation');
});


// Panolens
      let size = 250;
      let place = 200;
      let poeme11, poeme12, poeme13, poeme14, poeme15, poeme16, poeme17;
      let poeme21, poeme22, poeme23, poeme24, poeme25, poeme26, poeme27;
      let poeme31, poeme32, poeme33, poeme34, poeme35, poeme36, poeme37;
      let poeme41, poeme42, poeme43, poeme44, poeme45, poeme46, poeme47;
      let poeme51, poeme52, poeme53, poeme54, poeme55, poeme56, poeme57;
      let poeme61, poeme62, poeme63, poeme64, poeme65, poeme66, poeme67;
      let poeme71, poeme72, poeme73, poeme74, poeme75, poeme76, poeme77;

      // Zone 1
      poeme11 = new PANOLENS.Infospot( size, asterisque );
      poeme11.position.set( 4860.97, 1133.94, 143.18 );
      poeme11.addHoverElement( document.getElementById( '1-poeme-1' ), place );
      poeme12 = new PANOLENS.Infospot( size, asterisque );
      poeme12.position.set( 3961.55, 182.59, -3030.70 );
      poeme12.addHoverElement( document.getElementById( '1-poeme-2' ), place );
      poeme13 = new PANOLENS.Infospot( size, asterisque );
      poeme13.position.set( -4783.91, 1063.74, 963.52 );
      poeme13.addHoverElement( document.getElementById( '1-poeme-3' ), place );
      poeme14 = new PANOLENS.Infospot( size, asterisque );
      poeme14.position.set( 2885.04, 2322.59, 3350.56 );
      poeme14.addHoverElement( document.getElementById( '1-poeme-4' ), place );
      poeme15 = new PANOLENS.Infospot( size, asterisque );
      poeme15.position.set( -2779.93, 4144.20, 203.23 );
      poeme15.addHoverElement( document.getElementById( '1-poeme-5' ), place );
      poeme16 = new PANOLENS.Infospot( size, asterisque );
      poeme16.position.set( -3300.85, -446.56, -3723.95 );
      poeme16.addHoverElement( document.getElementById( '1-poeme-6' ), place );
      poeme17 = new PANOLENS.Infospot( size, asterisque );
      poeme17.position.set( 4060.24, -654.30, 2830.48 );
      poeme17.addHoverElement( document.getElementById( '1-poeme-7' ), place );

      // Zone 2
      poeme21 = new PANOLENS.Infospot( size, asterisque );
      poeme21.position.set( 2148.37, 3637.27, 2662.01 );
      poeme21.addHoverElement( document.getElementById( '2-poeme-1' ), place );
      poeme22 = new PANOLENS.Infospot( size, asterisque );
      poeme22.position.set( -3386.28, 1750.17, 3222.64 );
      poeme22.addHoverElement( document.getElementById( '2-poeme-2' ), place );
      poeme23 = new PANOLENS.Infospot( size, asterisque );
      poeme23.position.set( -208.83, 573.58, -4952.19 );
      poeme23.addHoverElement( document.getElementById( '2-poeme-3' ), place );
      poeme24 = new PANOLENS.Infospot( size, asterisque );
      poeme24.position.set( 3742.74, -772.67, 3216.40 );
      poeme24.addHoverElement( document.getElementById( '2-poeme-4' ), place );
      poeme25 = new PANOLENS.Infospot( size, asterisque );
      poeme25.position.set( -2265.42, -877.14, 4359.49 );
      poeme25.addHoverElement( document.getElementById( '2-poeme-5' ), place );
      poeme26 = new PANOLENS.Infospot( size, asterisque );
      poeme26.position.set( 2997.44, 1878.38, -3525.23 );
      poeme26.addHoverElement( document.getElementById( '2-poeme-6' ), place );
      poeme27 = new PANOLENS.Infospot( size, asterisque );
      poeme27.position.set( 4856.06, 1114.81, 309.39 );
      poeme27.addHoverElement( document.getElementById( '2-poeme-7' ), place );

      // Zone 3
      poeme31 = new PANOLENS.Infospot( size, asterisque );
      poeme31.position.set( 4727.76, 903.62, -1317.07 );
      poeme31.addHoverElement( document.getElementById( '3-poeme-1' ), place );
      poeme32 = new PANOLENS.Infospot( size, asterisque );
      poeme32.position.set( -74.31, 1138.83, 4863.60 );
      poeme32.addHoverElement( document.getElementById( '3-poeme-2' ), place );
      poeme33 = new PANOLENS.Infospot( size, asterisque );
      poeme33.position.set( -2976.01, 2412.01, 3203.37 );
      poeme33.addHoverElement( document.getElementById( '3-poeme-3' ), place );
      poeme34 = new PANOLENS.Infospot( size, asterisque );
      poeme34.position.set( -3425.47, 3313.85, -1499.51 );
      poeme34.addHoverElement( document.getElementById( '3-poeme-4' ), place );
      poeme35 = new PANOLENS.Infospot( size, asterisque );
      poeme35.position.set( -83.61, -157.64, -4989.42 );
      poeme35.addHoverElement( document.getElementById( '3-poeme-5' ), place );
      poeme36 = new PANOLENS.Infospot( size, asterisque );
      poeme36.position.set( 4437.71, 910.28, 2097.40 );
      poeme36.addHoverElement( document.getElementById( '3-poeme-6' ), place );
      poeme37 = new PANOLENS.Infospot( size, asterisque );
      poeme37.position.set( 740.28, 3679.37, -3296.01 );
      poeme37.addHoverElement( document.getElementById( '3-poeme-7' ), place );

      // Zone 4
      poeme41 = new PANOLENS.Infospot( size, asterisque );
      poeme41.position.set( 4781.14, 104.41, 1432.74 );
      poeme41.addHoverElement( document.getElementById( '4-poeme-1' ), place );
      poeme42 = new PANOLENS.Infospot( size, asterisque );
      poeme42.position.set( 3380.95, -1921.16, 3137.87 );
      poeme42.addHoverElement( document.getElementById( '4-poeme-2' ), place );
      poeme43 = new PANOLENS.Infospot( size, asterisque );
      poeme43.position.set( 672.75, -195.55, 4941.07 );
      poeme43.addHoverElement( document.getElementById( '4-poeme-3' ), place );
      poeme44 = new PANOLENS.Infospot( size, asterisque );
      poeme44.position.set( 657.98, 4025.41, -2887.92 );
      poeme44.addHoverElement( document.getElementById( '4-poeme-4' ), place );
      poeme45 = new PANOLENS.Infospot( size, asterisque );
      poeme45.position.set( -3055.15, -161.39, 3942.82 );
      poeme45.addHoverElement( document.getElementById( '4-poeme-5' ), place );
      poeme46 = new PANOLENS.Infospot( size, asterisque );
      poeme46.position.set( -4164.99, 1119.17, -2518.50 );
      poeme46.addHoverElement( document.getElementById( '4-poeme-6' ), place );
      poeme47 = new PANOLENS.Infospot( size, asterisque );
      poeme47.position.set( 1352.01, -690.38, -4754.61 );
      poeme47.addHoverElement( document.getElementById( '4-poeme-7' ), place );

      // Zone 5
      poeme51 = new PANOLENS.Infospot( size, asterisque );
      poeme51.position.set( 3595.85, 3356.61, -859.80 );
      poeme51.addHoverElement( document.getElementById( '5-poeme-1' ), place );
      poeme52 = new PANOLENS.Infospot( size, asterisque );
      poeme52.position.set( 4020.02, 993.85, 2787.45 );
      poeme52.addHoverElement( document.getElementById( '5-poeme-2' ), place );
      poeme53 = new PANOLENS.Infospot( size, asterisque );
      poeme53.position.set( -3142.11, 575.96, -3832.81 );
      poeme53.addHoverElement( document.getElementById( '5-poeme-3' ), place );
      poeme54 = new PANOLENS.Infospot( size, asterisque );
      poeme54.position.set( -1201.16, 4540.45, 1702.24 );
      poeme54.addHoverElement( document.getElementById( '5-poeme-4' ), place );
      poeme55 = new PANOLENS.Infospot( size, asterisque );
      poeme55.position.set( 150.03, 1996.17, 4573.38 );
      poeme55.addHoverElement( document.getElementById( '5-poeme-5' ), place );
      poeme56 = new PANOLENS.Infospot( size, asterisque );
      poeme56.position.set( -4420.96, -503.78, -2258.80 );
      poeme56.addHoverElement( document.getElementById( '5-poeme-6' ), place );
      poeme57 = new PANOLENS.Infospot( size, asterisque );
      poeme57.position.set( 2008.63, 1998.73, -4109.84 );
      poeme57.addHoverElement( document.getElementById( '5-poeme-7' ), place );

      // Zone 6
      poeme61 = new PANOLENS.Infospot( size, asterisque );
      poeme61.position.set( -4389.27, 15.17, -2382.62 );
      poeme61.addHoverElement( document.getElementById( '6-poeme-1' ), place );
      poeme62 = new PANOLENS.Infospot( size, asterisque );
      poeme62.position.set( -2787.15, 4110.35, 540.33 );
      poeme62.addHoverElement( document.getElementById( '6-poeme-2' ), place );
      poeme63 = new PANOLENS.Infospot( size, asterisque );
      poeme63.position.set( -1846, 3363.72, -3200.01 );
      poeme63.addHoverElement( document.getElementById( '6-poeme-3' ), place );
      poeme64 = new PANOLENS.Infospot( size, asterisque );
      poeme64.position.set( 2855.43, 645.21, -4045.31 );
      poeme64.addHoverElement( document.getElementById( '6-poeme-4' ), place );
      poeme65 = new PANOLENS.Infospot( size, asterisque );
      poeme65.position.set( 4023.32, 2250.13, 1923.50 );
      poeme65.addHoverElement( document.getElementById( '6-poeme-5' ), place );
      poeme66 = new PANOLENS.Infospot( size, asterisque );
      poeme66.position.set( 1397.54, 4344.49, -2030.36 );
      poeme66.addHoverElement( document.getElementById( '6-poeme-6' ), place );
      poeme67 = new PANOLENS.Infospot( size, asterisque );
      poeme67.position.set( 991.75, 574.16, 4860.96 );
      poeme67.addHoverElement( document.getElementById( '6-poeme-7' ), place );

      // Zone 7
      poeme71 = new PANOLENS.Infospot( size, asterisque );
      poeme71.position.set( -1541.04, -328.17, 4743.04 );
      poeme71.addHoverElement( document.getElementById( '7-poeme-1' ), place );
      poeme72 = new PANOLENS.Infospot( size, asterisque );
      poeme72.position.set( 4868.8, -1093.03, -149.02 );
      poeme72.addHoverElement( document.getElementById( '7-poeme-2' ), place );
      poeme73 = new PANOLENS.Infospot( size, asterisque );
      poeme73.position.set( 3614.97, 153.14, -3437.91 );
      poeme73.addHoverElement( document.getElementById( '7-poeme-3' ), place );
      poeme74 = new PANOLENS.Infospot( size, asterisque );
      poeme74.position.set( 456.08, -3128.44, -3866.60 );
      poeme74.addHoverElement( document.getElementById( '7-poeme-4' ), place );
      poeme75 = new PANOLENS.Infospot( size, asterisque );
      poeme75.position.set( -4031.29, -1120.74, 2723.81 );
      poeme75.addHoverElement( document.getElementById( '7-poeme-5' ), place );
      poeme76 = new PANOLENS.Infospot( size, asterisque );
      poeme76.position.set( 20.48, 4573.70, -2011.47 );
      poeme76.addHoverElement( document.getElementById( '7-poeme-6' ), place );
      poeme77 = new PANOLENS.Infospot( size, asterisque );
      poeme77.position.set( -4334.88, -2036.67, -1421.35 );
      poeme77.addHoverElement( document.getElementById( '7-poeme-7' ), place );

      let panorama1, panorama2, panorama3, panorama4, panorama5, panorama6, panorama7, viewer;

      panorama1 = new PANOLENS.ImagePanorama( zone1 );
      panorama1.add( poeme11 );
      panorama1.add( poeme12 );
      panorama1.add( poeme13 );
      panorama1.add( poeme14 );
      panorama1.add( poeme15 );
      panorama1.add( poeme16 );
      panorama1.add( poeme17 );

      panorama2 = new PANOLENS.ImagePanorama( zone2 );
      panorama2.add( poeme21 );
      panorama2.add( poeme22 );
      panorama2.add( poeme23 );
      panorama2.add( poeme24 );
      panorama2.add( poeme25 );
      panorama2.add( poeme26 );
      panorama2.add( poeme27 );

      panorama3 = new PANOLENS.ImagePanorama( zone3 );
      panorama3.add( poeme31 );
      panorama3.add( poeme32 );
      panorama3.add( poeme33 );
      panorama3.add( poeme34 );
      panorama3.add( poeme35 );
      panorama3.add( poeme36 );
      panorama3.add( poeme37 );

      panorama4 = new PANOLENS.ImagePanorama( zone4 );
      panorama4.add( poeme41 );
      panorama4.add( poeme42 );
      panorama4.add( poeme43 );
      panorama4.add( poeme44 );
      panorama4.add( poeme45 );
      panorama4.add( poeme46 );
      panorama4.add( poeme47 );

      panorama5 = new PANOLENS.ImagePanorama( zone5 );
      panorama5.add( poeme51 );
      panorama5.add( poeme52 );
      panorama5.add( poeme53 );
      panorama5.add( poeme54 );
      panorama5.add( poeme55 );
      panorama5.add( poeme56 );
      panorama5.add( poeme57 );

      panorama6 = new PANOLENS.ImagePanorama( zone6);
      panorama6.add( poeme61 );
      panorama6.add( poeme62 );
      panorama6.add( poeme63 );
      panorama6.add( poeme64 );
      panorama6.add( poeme65 );
      panorama6.add( poeme66 );
      panorama6.add( poeme67 );

      panorama7 = new PANOLENS.ImagePanorama( zone7 );
      panorama7.add( poeme71 );
      panorama7.add( poeme72 );
      panorama7.add( poeme73 );
      panorama7.add( poeme74 );
      panorama7.add( poeme75 );
      panorama7.add( poeme76 );
      panorama7.add( poeme77 );

      viewer = new PANOLENS.Viewer({
        autoHideInfospot: false
      });
      viewer.add( panorama1 );
      viewer.add( panorama2 );
      viewer.add( panorama3 );
      viewer.add( panorama4 );
      viewer.add( panorama5 );
      viewer.add( panorama6 );
      viewer.add( panorama7 );

      // Linking between panoramas
      panorama1.link( panorama2, new THREE.Vector3( 2779.69, -59.12, -4145.73 ), size, chemin );
      panorama2.link( panorama3, new THREE.Vector3( 4644.85, -347.22, -1794.77 ), size, chemin );
      panorama3.link( panorama4, new THREE.Vector3( 3611.28, 259.90, -3435.68 ), size, chemin );
      panorama4.link( panorama5, new THREE.Vector3( 4524.53, 507.11, -2054.67 ), size, chemin );
      panorama5.link( panorama6, new THREE.Vector3( 4500.73, 257.65, -2142.91 ), size, chemin );
      panorama6.link( panorama7, new THREE.Vector3( -1272.63, 3269.64, 3556.28 ), size, chemin );
      panorama7.link( panorama1, new THREE.Vector3( 3591.31, 1376.37, 3186.47 ), size, chemin );

      panorama7.link( panorama6, new THREE.Vector3( -2273.06, 614.38, -4399.02 ), size, chemin );
      panorama6.link( panorama5, new THREE.Vector3( -1659.98, 118.49, -4706.20 ), size, chemin );
      panorama5.link( panorama4, new THREE.Vector3( -4992.78, 171.36, 9.26 ), size, chemin );
      panorama4.link( panorama3, new THREE.Vector3( -3550.45, -903.50, 3389.90 ), size, chemin );
      panorama3.link( panorama2, new THREE.Vector3( -4602.66, 82.72, 1933.62 ), size, chemin );
      panorama2.link( panorama1, new THREE.Vector3( -1145.54, 291.43, 4850.50 ), size, chemin );
      panorama1.link( panorama7, new THREE.Vector3( -1424.21, 1898.17, 4400.00 ), size, chemin );


// PANOLENS.Viewer.prototype.getPosition = function () {
//   var intersects, point, panoramaWorldPosition, outputPosition;
//   intersects = this.raycaster.intersectObject( this.panorama, true );
//
//   if ( intersects.length > 0 ) {
//     point = intersects[0].point;
//     panoramaWorldPosition = this.panorama.getWorldPosition();
//
//     // Panorama is scaled -1 on X axis
//     outputPosition = new THREE.Vector3(
//       -(point.x - panoramaWorldPosition.x).toFixed(2),
//       (point.y - panoramaWorldPosition.y).toFixed(2),
//       (point.z - panoramaWorldPosition.z).toFixed(2)
//     );
//   }
//
//   return outputPosition;
// };
//
// panorama1.addEventListener( 'click', function(){
//     var clickPosition = viewer.getPosition(); //returns x,y,z
//     console.log(" " + clickPosition.x + ", " + clickPosition.y + ", " + clickPosition.z + " ");
// });
// panorama2.addEventListener( 'click', function(){
//     var clickPosition = viewer.getPosition(); //returns x,y,z
//     console.log(" " + clickPosition.x + ", " + clickPosition.y + ", " + clickPosition.z + " ");
// });
// panorama3.addEventListener( 'click', function(){
//     var clickPosition = viewer.getPosition(); //returns x,y,z
//     console.log(" " + clickPosition.x + ", " + clickPosition.y + ", " + clickPosition.z + " ");
// });
// panorama4.addEventListener( 'click', function(){
//     var clickPosition = viewer.getPosition(); //returns x,y,z
//     console.log(" " + clickPosition.x + ", " + clickPosition.y + ", " + clickPosition.z + " ");
// });
// panorama5.addEventListener( 'click', function(){
//     var clickPosition = viewer.getPosition(); //returns x,y,z
//     console.log(" " + clickPosition.x + ", " + clickPosition.y + ", " + clickPosition.z + " ");
// });
// panorama6.addEventListener( 'click', function(){
//     var clickPosition = viewer.getPosition(); //returns x,y,z
//     console.log(" " + clickPosition.x + ", " + clickPosition.y + ", " + clickPosition.z + " ");
// });
// panorama7.addEventListener( 'click', function(){
//     var clickPosition = viewer.getPosition(); //returns x,y,z
//     console.log(" " + clickPosition.x + ", " + clickPosition.y + ", " + clickPosition.z + " ");
// });

// Musique
var audio = document.getElementById("fond-sonore");
var musiqueJoue = "pause";
function fondSonore() {
    if (audio.paused) {
        audio.play();
    }
    else {
        audio.pause();
    }
};

document.querySelector('#musique').onclick = function () {
  if (musiqueJoue == "pause") {
    musiqueJoue = "continue";
  } else {
    musiqueJoue = "pause";
  };
  // document.querySelector('#musique .icone--music').classList.toggle('rotation');
  document.querySelector('#musique .icone--music-on').classList.toggle('none');
  document.querySelector('#musique .icone--music-off').classList.toggle('none');
  fondSonore();
}


let touchEvent = 'ontouchstart' in window ? 'touchstart' : 'click';
let permissionValue = false;
let messageIOS = false;
// document.querySelector('.panolens-container div span:nth-child(1) a').addEventListener("click touchstart", function() {
document.querySelector('.panolens-container div span:nth-child(1) a').addEventListener(touchEvent, function() {
  // document.querySelector('.panolens-container div:nth-child(3) span:nth-child(5) a:nth-child(3)').addEventListener(touchEvent, function() {
// window.onload = function () {
  // alert("hello one");
  // Check if is IOS 13 when page loads.
  // Source : https://github.com/aframevr/aframe/issues/4287#issuecomment-548208060

    const platform = window.navigator.platform;
    const iosPlatforms = ['iPhone', 'iPad', 'iPod'];

  if ( !permissionValue && window.DeviceMotionEvent && typeof window.DeviceMotionEvent.requestPermission === 'function' ){
      // Everything here is just a lazy banner. You can do the banner your way.
      const banner = document.createElement('div')
    banner.innerHTML = `<div class="devicemotion" style="z-index: 900; position: absolute; bottom: 0; left: 0; width: 100%; background-color:#000; color: #fff;"><p style="font-style: 1em; padding: 2em 1em; text-align: center;">Cliquer sur cette bannière<br>pour activer le «&nbsp;Device Motion&nbsp;» (gyroscope) sur iOS<br>(effacer les données de navigation<br>pour changer de décision).</p></div>`
      banner.onclick = ClickRequestDeviceMotionEvent // You NEED to bind the function into a onClick event. An artificial 'onClick' will NOT work.
    document.querySelector('body').appendChild(banner)
  } else if (!messageIOS && iosPlatforms.indexOf(platform) !== -1) {
      const banner = document.createElement('div')
    banner.innerHTML = `<div class="devicemotion--ios" style="z-index: 900; position: absolute; bottom: 0; left: 0; width: 100%; background-color:#000; color: #fff;"><p style="font-style: 1em; padding: 2em 1em; text-align: center;">Pour les versions inférieures à iOS 13<br>activer le «&nbsp;Device Motion&nbsp;» (gyroscope)<br>dans Réglages > Safari > Confidentialité et Sécurité > Mouvement et Orientation<br>(cliquer sur cette bannière<br>pour la faire disparaître).</p></div>`
    banner.onclick = () => banner.remove(); // You NEED to bind the function into a onClick event. An artificial 'onClick' will NOT work.
    document.querySelector('body').appendChild(banner)
    messageIOS = true;
  }
  // });
});

function ClickRequestDeviceMotionEvent () {
  window.DeviceMotionEvent.requestPermission()
    .then(response => {
      if (response === 'granted') {
        window.addEventListener('devicemotion',
          () => { console.log('DeviceMotion permissions granted.') },
          (e) => { throw e }
      )
      } else {
        console.log('DeviceMotion permissions not granted.')
      }
      permissionValue = true;
      document.querySelector(".devicemotion").remove();
    })
    .catch(e => {
      console.error(e)
    })
}

// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

// Prevent bounce effect iOS
// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
var content = document.querySelector('.pagetitre');
content.addEventListener('touchstart', function (event) {
    this.allowUp = (this.scrollTop > 0);
    this.allowDown = (this.scrollTop < this.scrollHeight - this.clientHeight);
    this.slideBeginY = event.pageY;
});

content.addEventListener('touchmove', function (event) {
    var up = (event.pageY > this.slideBeginY);
    var down = (event.pageY < this.slideBeginY);
    this.slideBeginY = event.pageY;
    if ((up && this.allowUp) || (down && this.allowDown)) {
        event.stopPropagation();
    }
    else {
        event.preventDefault();
    }
});

