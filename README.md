# ~/ABRÜPT/LUCIEN RAPHMAJ/BLANDINE VOLOCHOT/*

La [page de ce livre](https://abrupt.ch/lucien-raphmaj/blandine-volochot/) sur le réseau.

## Sur le livre

Échappée de son impossible laboratoire spatial, Blandine Volochot est une tentative poétique d’écrire la [critique fiction](https://lucienraphmaj.wordpress.com/2017/05/10/que-peut-la-critique/) de deux univers mutés l’un en l’autre. Mais plutôt qu'une traduction belle infidèle des concepts et idées de Blanchot et de Volodine, Blandine Volochot est une créature rebelle s’émancipant dans son chemin poétique où, en douze chapitres, en plusieurs morts, en plusieurs amours, en péripéties et en aberrations elle se révolutionne dans tous les sens.

Lire et délire. Rêver et proposer des alternatives poétiques et critiques. Passer [de l’essai à l’essaim.](https://lucienraphmaj.wordpress.com/2017/06/24/de-lessai-a-lessaim/) Satelliser la critique en fiction. Proposer des lectures comme des lignes de fuite. Ce sont autant de rayons de l’étoile désastrée qu’est Blandine Volochot.

## Sur l'auteur

On pourrait dire *Lucien Blandinovitch Raphmaj*, pour lui donner un air de Victor Frankenstein enfanté par sa créature, mais il a déjà une vie de latérateur. Écrivant des textes comme des traversées des arts et des savoirs, il pratique avec [SMITH](http://smith.pictures/) ces transformations contemporaines de l’imaginaire par les mots.

([Son espace](https://lucienraphmaj.wordpress.com/) de latérature, quelque part parmi les réseaux.)


## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

Les images originales de cet antilivre sont celles de l'ESO ( disponibles sous licence *CC-BY*) :

- <a href="https://www.eso.org/public/images/chajnantor-wide-pan2-ext/" target="_blank">ESO</a>
- <a href="https://www.eso.org/public/images/2017_11_16_La_Silla_FD_12mm_MW-eq-CC/" target="_blank">ESO/P. Horálek</a>
- <a href="https://www.eso.org/public/images/brammer-day-eq-extended/" target="_blank">ESO/G. Brammer</a>
- <a href="https://www.eso.org/public/images/potw1405a-ext/" target="_blank">ESO/G. Brammer & F. Kerber</a>
- <a href="https://www.eso.org/public/images/2016_04_09_ALMA_Dusk_FD360_Pano_Horalek-CC-ext/" target="_blank">ESO/P. Horálek</a>
- <a href="https://www.eso.org/public/images/uhd-img3219pv2-cc-eq-extended/" target="_blank">ESO/B. Tafreshi</a>
- <a href="https://www.eso.org/public/images/eso0932a/" target="_blank">ESO/S. Brunier</a>

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).

